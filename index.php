<?php
$bootstrap_path = __DIR__ . '/webdata/bootstrap.php';
include($bootstrap_path);
$helper = new Helper();
if (! $user_id = $_SESSION['user_id']) {
	$helper->redirect('login.php');
}

$user_model = new User();
$users = $user_model->search(['id' => $user_id]);
$user = $users[0];
?>

歡迎 <?= htmlspecialchars($user->name) ?>

