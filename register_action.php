<?php
$bootstrap_path = __DIR__ . '/webdata/bootstrap.php';
include($bootstrap_path);

$name = $_POST['name'];
if (empty($name)) {
	echo '缺少資料: 帳號' . PHP_EOL;
	exit;
}
$password = $_POST['password'];
if (empty($password)) {
	echo '缺少資料: 密碼' . PHP_EOL;
	exit;
}
$password = password_hash($password, PASSWORD_DEFAULT);

$post['name'] = $name;
$post['password'] = $password;
$post['reg_at'] = time();

$user_model = new User();
$ret = $user_model->insert($post);

$helper = new Helper();
$helper->redirect('login.php');
