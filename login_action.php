<?php
$bootstrap_path = __DIR__ . '/webdata/bootstrap.php';
include($bootstrap_path);

if (! $name = $_POST['name']) {
	echo '缺少參數: 帳號';
	exit;
}
if (! $password = $_POST['password']) {
	echo '缺少參數: 密碼';
	exit;
}

$user_model = new User();
$users = $user_model->search(['name' => $name]);
$user = $users[0];

if (!password_verify($password, $user->password)) {
	echo '密碼錯誤';
	exit;
}

$_SESSION['user_id'] = $user->id;
$helper = new Helper();
$helper->redirect('index.php');
