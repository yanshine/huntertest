<?php
class User extends Table
{
	public $_columns = [];
	public function __construct()
	{
		$this->_name = 'user';
		$this->_primary = 'id';

		$this->_columns['id'] = ['type' => 'int', 'size' => 10, 'unsigned' => true, 'auto_increment' => true];
		$this->_columns['name'] = ['type' => 'varchar', 'size' => 32, 'default' => ''];
		$this->_columns['password'] = ['type' => 'text'];
		$this->_columns['avatar'] = ['type' => 'varchar', 'size' => 50];

		$this->db = DbLib::getSingleLink();
	}
}
