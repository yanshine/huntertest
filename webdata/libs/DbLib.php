<?php
class DbLib
{
	public static function getSingleLink()
	{
		$servername = "localhost";
		$username = "webuser";
		$password = "webuserpass";
		$dbname = 'web';

		$connect = mysqli_connect($servername, $username, $password, $dbname);
		return $connect;
	}
}
