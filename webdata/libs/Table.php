<?php
class Table
{
	public function insert($data = [])
	{
		// XXX 應該要檢查欄位是否存在
		foreach ($data as $column => $value) {
			$columns[] = $column;
			$values[] = "'" . mysqli_real_escape_string($this->db, $value) . "'";
		}

		$query = "INSERT INTO {$this->_name} (" . implode(',', $columns) .") VALUES (" . implode(',', $values) . ")";
		$this->query($query);
	}

	public function update($data = [])
	{
		$id = $data['id'];
		foreach ($data as $column => $value) {
			if ('id' == $column) {
				continue;
			}
			$value = "'" . mysqli_real_escape_string($this->db, $value) . "'";
			$query_string[] = "{$column} = {$value}";
		}
		$query = "UPDATE {$this->_name} SET " . implode(',', $query_string) . "WHERE id = {$id}";
		$this->query($query);
	}

	public function search($data = [])
	{
		foreach ($data as $column => $value) {
			$value = "'" . mysqli_real_escape_string($this->db, $value) . "'";
			$query_string[] = "{$column} = {$value}";
		}

		$query = "SELECT * FROM {$this->_name} WHERE " . implode(' AND ', $query_string);
		return $this->query($query);
	}

	public function query($query)
	{
		try {
			$ret = mysqli_query($this->db, $query);
		} catch (Exception $e) {
			trigger_error("Table {$this->_name} query error, msg:" . strval($e), E_USER_WARNING);
		}
		if (! empty($ret->num_rows)) {
			$rows = [];
			while($row = $ret->fetch_object()) {
				$rows[] = $row;
			}
			return $rows;
		}
	}
}
