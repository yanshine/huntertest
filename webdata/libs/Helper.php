<?php
class Helper
{
	public function redirect($uri)
	{
		header('Location: '. $uri);
		exit();
	}
}
